
package app.io
{
	
	import app.managers.UIManager;
	import flash.errors.*;
	import flash.events.*;
	import flash.net.*;
	import vk.api.serialization.json.*;
	
	/**
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	
	public class DataProvider
	{
		private var _api_url:String = "http://svoeradio/api/";
		private var _api_id:Number;
		private var _viewer_id:Number;
		private var _auth_key:String;
		private var _request_params:Array;
		
		private var _global_options:Object;
		
		public function DataProvider(api_url:String, api_id:Number, viewer_id:Number, auth_key:String)
		{
			_api_id = api_id;
			_viewer_id = viewer_id;
			_auth_key = auth_key;
			_api_url = api_url;
		}
		
		public function request(method:String, options:Object = null):void
		{
			var onComplete:Function, onError:Function;
			if (options == null)
			{
				options = new Object();
			}
			options.onComplete = options.onComplete ? options.onComplete : (_global_options.onComplete ? _global_options.onComplete : null);
			options.onError = options.onError ? options.onError : (_global_options.onError ? _global_options.onError : null);
			_sendRequest(method, options);
		}
		
		/********************
		 * Private methods
		 ********************/
		
		private function _sendRequest(method:String, options:Object):void
		{
			var self:Object = this;
			
			var request_params:Object = {"method": method};
			request_params.api_id = _api_id;
			request_params.viewer_id = _viewer_id;
			request_params.format = "JSON";
			request_params.auth_key = _auth_key;
			request_params.v = "3.0";
			request_params.rand = Math.random();
			if (options.params)
			{
				for (var i:String in options.params)
				{
					request_params[i] = options.params[i];
				}
			}
			
			var variables:URLVariables = new URLVariables();
			for (var j:String in request_params)
			{
				variables[j] = request_params[j];
			}
			var request:URLRequest = new URLRequest();
			request.url = _api_url;
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			if (options.onError)
			{
				loader.addEventListener(IOErrorEvent.IO_ERROR, function():void
				{
					var o:Object = { "error_code":UIManager.ERROR_CODE_PROVIDER, "error_msg":"Connection error" };
					options.onError(o);
				});
				loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, function():void
				{
					var o:Object = { "error_code":UIManager.ERROR_CODE_PROVIDER, "error_msg":"Security error occured" };
					options.onError(o);
				});
			}
			
			loader.addEventListener(Event.COMPLETE, function(e:Event):void
			{
				var loader:URLLoader = URLLoader(e.target);
				trace(loader.data);
				var data:Object = decode(loader.data);
				if (data.error)
				{
					options.onError(data.error);
				}
				else if (options.onComplete && data.response)
				{
					options.onComplete(data.response);
				}
			});
			try
			{
				loader.load(request);
			}
			catch (error:Error)
			{
				options.onError(error);
			}
		}
		
		private function decode(data:String):Object
		{
			var decoder:JSONDecoder = new JSONDecoder(data);
			return decoder.getValue();
		}
	}
}