package app.io
{
	import app.io.DataProvider;
	import flash.events.*;
	import vk.events.*;
	
	/**
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class ServerAPI extends EventDispatcher
	{
		
		private var dp:DataProvider;
		
		public function ServerAPI(... params)
		{
			var api_url:String = 'http://svoeradio/api/';
			
			if (params[0].api_result && params[0].api_result.server_url && (params[0].api_result.server_url.length > 0))
			{
				api_url = params[0].api_result.server_url;
			}
			
			params[0].api_url = api_url;
			dp = new DataProvider(params[0].api_url, params[0].api_id, params[0].viewer_id, params[0].auth_key);
		}
		
		public function api(method:String, params:Object, onComplete:Function = null, onError:Function = null):void
		{
			var options:Object = new Object();
			options['params'] = params;
			options['onComplete'] = onComplete;
			options['onError'] = onError;
			dp.request(method, options);
		}
	}
}
