package app.misc
{
	
	public class Messages
	{
		public static const EMPT_STRING:String = '';
		
		/*
		 * Оповещения - отображаются в статусной строке
		 */
		
		public static const NOTICE_WELCOME_TO_THE_JUNGLE:String = 'Добро пожаловать в Своё Радио!';
		public static const NOTICE_PLAYING:String = 'Воспроизведение...';
		public static const NOTICE_VOLUME_CHANGE:String = 'Громкость: %d%.';
		public static const NOTICE_LOAD_STREAM:String = 'Загрузка потока...';
		public static const NOTICE_LOAD_TRACK:String = 'Загрузка трека...';
		public static const NOTICE_BUFFERING:String = 'Буферизация...';
		public static const NOTICE_TRACK_END:String = 'Завершено воспроизведение трека.';
		
		/*
		 * Сообщения - отображаются во всплывающем окне с кнопкой "ОК"
		 */
		public static const MESSAGE_ADDED_TO_BOOKMARKS:String = 'Станция добавлена в закладки.';
		public static const MESSAGE_REMOVED_FROM_BOOKMARKS:String = 'Станция удалена из закладок.';
		public static const MESSAGE_TRACK_ADDED:String = 'Трек добавлен в Ваши аудиозаписи.';
		public static const MESSAGE_SELECT_LIST_STATION:String = 'Выберите станцию из списка.';
		public static const MESSAGE_STATION_SUCC_CREAT:String = 'Поздравляем! Станция создана успешно!';
		public static const MESSAGE_NO_GROUPS:String = 'Создайте группу для новой станции и перезагрузите приложение.';
		public static const MESSAGE_EMPTY_SNAME:String = 'Заполните поле "Название станции".';
		public static const MESSAGE_TRACK_DELETED:String = 'Невозможно воспроизвести удаленный трек.';
		public static const MESSAGE_OFFER_GENRE:String = 'Нажмите на кнопку "обратная связь" снизу слева. Откроется обсуждение, в котором вы можете предложить жанр.';
		
		/*
		 * Ошибки - отображаются в критическом окне
		 */
		
		public static const ERROR_SERVER:String = 'Произошла ошибка сервера. Перезапустите приложение.';
		public static const ERROR_VK:String = 'Произошла ошибка при запросе к ВКонтакте API. Перезапустите приложение или зайдите позже.';
		public static const ERROR_UNKNOWN:String = 'Произошла неизвестная ошибка, сообщите её в группе. Данные: %s';
		public static const ERROR_AUDIO_IO:String = 'Произошла ошибка сети, возможно не работает сеть.';
		public static const ERROR_SESSION_EXPIRED:String = 'Сессия истекла. Перезагрузите страницу с приложением.';
		public static const ERROR_CONFIG_NOT_LOADED:String = 'Конфигурация не загружена.';
		public static const ERROR_INVALID_CONFIG:String = 'Загружена некорректная конфигурация.';
	}
}