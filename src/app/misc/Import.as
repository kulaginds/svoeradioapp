package app.misc
{
	import flash.display.BitmapData;
	
	public class Import
	{
		// Фон интерфейса.
		[Embed(source = "../../../assets/background.png")]
		private static const background_ui:Class;
		public static const background:BitmapData = new background_ui().bitmapData;
		
		// Иконки добавления/удаления закладок.
		[Embed(source = "../../../assets/add_bookmark.png")]
		private static const add_bkmk:Class;
		public static const add_bookmark:BitmapData = new add_bkmk().bitmapData;
		
		[Embed(source = "../../../assets/remove_bookmark.png")]
		private static const rmv_bkmk:Class;
		public static const remove_bookmark:BitmapData = new rmv_bkmk().bitmapData;
		
		// Иконка добавления трека в аудиозаписи.
		[Embed(source = "../../../assets/add_audio.png")]
		private static const add_aud:Class;
		public static const add_audio:BitmapData = new add_aud().bitmapData;
		
		// Иконка кнопки обратной связи.
		[Embed(source = "../../../assets/feedback_btn.png")]
		private static const feedb_bt:Class;
		public static const feedback_btn:BitmapData = new feedb_bt().bitmapData;
		
		// Иконка кнопки сообщения багов.
		[Embed(source = "../../../assets/bug_btn.png")]
		private static const bug_bt:Class;
		public static const bug_btn:BitmapData = new bug_bt().bitmapData;
		
		// Иконки звука.
		[Embed(source = "../../../assets/sound_off.png")]
		private static const snd_off:Class;
		public static const sound_off:BitmapData = new snd_off().bitmapData;
		
		[Embed(source = "../../../assets/sound_on.png")]
		private static const snd_on:Class;
		public static const sound_on:BitmapData = new snd_on().bitmapData;
		
		// Иконка ползунка громкости.
		[Embed(source = "../../../assets/volume_runner.png")]
		private static const vol_runn:Class;
		public static const vol_runner:BitmapData = new vol_runn().bitmapData;
		
		// Иконка для кнопки "закрыть".
		[Embed(source = "../../../assets/close_btn.png")]
		private static const clos_btn:Class;
		public static const close_btn:BitmapData = new clos_btn().bitmapData;
		
		// Иконки управления воспроизведением.
		[Embed(source = "../../../assets/play.png")]
		private static const pl:Class;
		public static const play:BitmapData = new pl().bitmapData;
		
		[Embed(source = "../../../assets/stop.png")]
		private static const st:Class;
		public static const stop:BitmapData = new st().bitmapData;
		
		// Фоны кнопок меню.
		// Кнопка поиск.
		[Embed(source = "../../../assets/search_tab_active.png")]
		private static const search_tab_activ:Class;
		public static const search_tab_active:BitmapData = new search_tab_activ().bitmapData;
		
		[Embed(source = "../../../assets/search_tab_deactive.png")]
		private static const search_tab_deactiv:Class;
		public static const search_tab_deactive:BitmapData = new search_tab_deactiv().bitmapData;
		
		// Кнопка закладок.
		[Embed(source = "../../../assets/bookmarks_tab_active.png")]
		private static const bookmarks_tab_activ:Class;
		public static const bookmarks_tab_active:BitmapData = new bookmarks_tab_activ().bitmapData;
		
		[Embed(source = "../../../assets/bookmarks_tab_deactive.png")]
		private static const bookmarks_tab_deactiv:Class;
		public static const bookmarks_tab_deactive:BitmapData = new bookmarks_tab_deactiv().bitmapData;
		
		// Кнопка создать.
		[Embed(source = "../../../assets/create_tab_active.png")]
		private static const create_tab_activ:Class;
		public static const create_tab_active:BitmapData = new create_tab_activ().bitmapData;
		
		[Embed(source = "../../../assets/create_tab_deactive.png")]
		private static const create_tab_deactiv:Class;
		public static const create_tab_deactive:BitmapData = new create_tab_deactiv().bitmapData;
		
		/*
		 * Рейтинг.
		 */
		
		// Кнопка минус.
		[Embed(source = "../../../assets/rating_minus.png")]
		private static const rat_min:Class;
		public static const rating_minus:BitmapData = new rat_min().bitmapData;
		
		// Кнопка плюс.
		[Embed(source = "../../../assets/rating_plus.png")]
		private static const rat_pl:Class;
		public static const rating_plus:BitmapData = new rat_pl().bitmapData;
		
		// Кнопка активной звезды.
		[Embed(source = "../../../assets/rating_active_star.png")]
		private static const act_star:Class;
		public static const rating_active_star:BitmapData = new act_star().bitmapData;
		
		// Кнопка неактивной звезды.
		[Embed(source = "../../../assets/rating_deactive_star.png")]
		private static const deact_star:Class;
		public static const rating_deactive_star:BitmapData = new deact_star().bitmapData;
		
		// Кнопка сообщения
		[Embed(source = "../../../assets/message.png")]
		private static const msg_star:Class;
		public static const message:BitmapData = new msg_star().bitmapData;
	}
}