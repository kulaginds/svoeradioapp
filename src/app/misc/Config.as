package app.misc
{
	import app.ui.text.TF;
	
	/**
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Config
	{
		public static const APP_CONFIG_URL:String = '../appConfig.json';
		public static const RULES_URL:String = 'http://vk.com/page-52497884_44468465';
		public static const FEEDBACK_URL:String = 'http://vk.com/topic-52497884_30997212';
		public static const PANEL_URL:String = 'http://vk.com/app3624062_-';
		public static const FLEX_TF:TF = new TF();
		public static const FIRST_VK_QUERY:String = 'return {"groups":API.groups.get({extended:1,filter:"editor"}),"volume":API.storage.get({key:"volume"}),"station":API.storage.get({key:"station"}),"favorite":API.storage.get({key:"favorite"})};';
		public static const DEFAULT_VOLUME:Number = 0.5;
		public static const STATUS_MOMENT_TIME:int = 3000; // 3 сек
	}
}