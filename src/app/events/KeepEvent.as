package app.events
{
	import flash.events.Event;
	
	/**
	 * Событие KeepContrller
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class KeepEvent extends Event
	{
		public static const FIRST_LOAD:String = 'first load';
		
		public function KeepEvent(type:String)
		{
			super(type);
		}
		
		public override function clone():Event
		{
			return new KeepEvent(type);
		}
		
		public override function toString():String
		{
			return formatToString("KeepControllerEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	
	}

}