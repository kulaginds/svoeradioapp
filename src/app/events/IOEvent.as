package app.events
{
	import flash.events.Event;
	
	/**
	 * Событие ответа LoadController-а.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class IOEvent extends Event
	{
		public static const FIRST_LOAD:String = 'first load';
		public static const ADD_AUDIO:String = 'add audio';
		public static const GET_BROADCAST:String = 'get broadcast';
		public static const GET_STATION:String = 'get station';
		public static const RATE:String = 'rate';
		public static const SEARCH:String = 'search';
		public static const GET_STATIONS:String = 'get stations';
		public static const CREATE:String = 'create';
		public static const GET_MY:String = 'get my';
		public static const ERROR:String = 'error';
		
		private var _data:Object;
		
		public function IOEvent(type:String, data:Object)
		{
			super(type);
			_data = data;
		}
		
		public function get data():Object
		{
			return _data;
		}
		
		public override function clone():Event
		{
			return new IOEvent(type, _data);
		}
		
		public override function toString():String
		{
			return formatToString("LoadControllerEvent", "type", "bubbles", "cancelable", "eventPhase");
		}
	
	}

}