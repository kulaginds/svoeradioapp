package app.events
{
	import flash.events.Event;
	
	/**
	 * Событие AudioController.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class AudioEvent extends Event
	{
		public static const VOLUME_CHANGE:String = 'volume change';
		public static const PLAY:String = 'play';
		public static const STOP:String = 'stop';
		public static const ERROR:String = 'error';
		public static const SOUND_COMPLETE:String = 'sound complete';
		
		public function AudioEvent(type:String)
		{
			super(type);
		}
		
		public override function clone():Event
		{
			return new AudioEvent(type);
		}
	
	}

}