package app.managers
{
	import app.misc.Config;
	import app.misc.Messages;
	import app.ui.containers.Head;
	import app.ui.containers.MessageWindow;
	import app.ui.containers.StatusLine;
	import app.ui.rating.RatingEvent;
	import app.ui.tabs.BookmarksTab;
	import app.ui.tabs.CreateTab;
	import app.ui.tabs.SearchTab;
	import app.ui.volume.VolumeEvent;
	import fl.data.DataProvider;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * Управляет пользовательским интерфейсом.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class UIManager
	{
		public static const ERROR_CODE_NOTICE:int = 100;
		public static const ERROR_CODE_MESSAGE:int = 101;
		public static const ERROR_CODE_API:int = 300;
		public static const ERROR_CODE_CRITICAL:int = 200;
		public static const ERROR_CODE_PROVIDER:int = 400;
		
		private static var head:Head; // ссылка на контейнер головы
		private static var search:SearchTab; // ссылка на контейнер вкладки поиска
		private static var bookmarks:BookmarksTab; // ссылка на контейнер вкладки закладок
		private static var create:CreateTab; // ссылка на контейнер вкладки создать
		
		/*
		 * init
		 * 
		 * Производит инициализацию менеджера.
		 */
		public static function init():void
		{
			// Инициализируем переменные контейнеров, чтобы к ним проще было обращаться (меньше кода)
			var main:SvoeRadioApp = SvoeRadioApp.instance();
			head = main.head;
			search = main.content.search_tab;
			bookmarks = main.content.bookmarks_tab;
			create = main.content.create_tab;
			
			/*
			 * Подключаем обработчики к графическим элементам.
			 */
			
			head.bookmark_btn.addEventListener(MouseEvent.CLICK, onBookmarkBtnClick);
			head.audio_btn.addEventListener(MouseEvent.CLICK, onAudioBtnClick);
			head.volume.addEventListener(VolumeEvent.CHANGE, onVolumeChange);
			head.play_btn.addEventListener(MouseEvent.CLICK, onPlayBtnClick);
			head.stop_btn.addEventListener(MouseEvent.CLICK, onStopBtnClick);
			head.rating.addEventListener(RatingEvent.CLICK, onRatingStarClick);
			
			main.bug_btn.addEventListener(MouseEvent.CLICK, onBugBtnClick);
			main.feedback_btn.addEventListener(MouseEvent.CLICK, onFeedbackBtnClick);
			
			search.search_btn.addEventListener(MouseEvent.CLICK, onSearchBtnClick);
			search.stations_list.addEventListener(Event.CHANGE, onStationListChange);
			search.stations_list.addEventListener(MouseEvent.DOUBLE_CLICK, onPlayBtnClick);
			search.play_btn.addEventListener(MouseEvent.CLICK, onTabPlayBtnClick);
			search.bookmark_btn.addEventListener(MouseEvent.CLICK, onTabBookmarkBtnClick);
			search.group_btn.addEventListener(MouseEvent.CLICK, onTabGroupBtnClick);
			
			bookmarks.search_btn.addEventListener(MouseEvent.CLICK, onBookmarksTabSearchBtnClick);
			bookmarks.stations_list.addEventListener(Event.CHANGE, onStationListChange);
			bookmarks.stations_list.addEventListener(MouseEvent.DOUBLE_CLICK, onPlayBtnClick);
			bookmarks.play_btn.addEventListener(MouseEvent.CLICK, onTabPlayBtnClick);
			bookmarks.bookmark_btn.addEventListener(MouseEvent.CLICK, onTabBookmarkBtnClick);
			bookmarks.group_btn.addEventListener(MouseEvent.CLICK, onTabGroupBtnClick);
			
			create.create_btn.addEventListener(MouseEvent.CLICK, onCreateBtnClick);
			create.panel_btn.addEventListener(MouseEvent.CLICK, onPanelBtnClick);
			create.select_btn.addEventListener(MouseEvent.CLICK, onSelectStationClick);
			create.offer_genre_btn.addEventListener(MouseEvent.CLICK, onCreateOfferGenreBtnClick);
		}
		
		/*
		 * firstLoad
		 * 
		 * Метод первичной загрузки приложения (отсюда начинается работа всего приложения).
		 * Устанавливает текущую станцию, рейтинг, списки, слайдер вкладки создать, громкость и скрывает окно загрузки.
		 */
		public static function firstLoad(data:Object):void
		{
			setStationName(data.station.name, false);
			setRating(data.station.rating);
			
			create.group.dataProvider = data.groups;
			search.genres_box.dataProvider = data.genres;
			bookmarks.genres_box.dataProvider = data.genres;
			create.genre.dataProvider = data.genres2;
			create.station.dataProvider = data.my_stations;
			setSearchListDp(data.stations);
			setBookmarksListDp(data.favorite);
			
			create.setSlide(data.isOwner);
			
			setVolume(Number(data.volume));
			
			hideLoading();
		}
		
		/*
		 * updateBookmarkButton
		 * 
		 * Изменяет состояние кнопки избранного в голове.
		 */
		public static function updateBookmarkButton(station_id:int, reverse:Boolean = false):Boolean
		{
			var is_favor:Boolean = KeepManager.isFavorite(station_id);
			if (reverse) {
				is_favor ? KeepManager.removeFavorite(station_id) : KeepManager.addFavorite(station_id);
				is_favor = !is_favor;
			}
			head.bookmark_btn.setState(is_favor ? 1 : 0);
			return is_favor;
		}
		
		/*
		 * setRating
		 * 
		 * Изменяет графическое отображение рейтинга в голове.
		 */
		public static function setRating(value:Number):void
		{
			head.rating.setRating(value);
		}
		
		/*
		 * setVolume
		 * 
		 * Изменяет графическое отображение громкости в голове.
		 */
		public static function setVolume(value:Number):void
		{
			head.volume.value = value;
		}
		
		/*
		 * setStationName
		 * 
		 * Устанавливает название текущей станции и если надо, активирует его.
		 */
		public static function setStationName(name:String, active:Boolean = true):void
		{
			head.station_name.setText(name);
			if (active)
				head.station_name.play();
			else
				head.station_name.stop();
		}
		
		/*
		 * playStationName
		 * 
		 * Активирует название текущей станции.
		 */
		public static function playStationName():void
		{
			head.station_name.play();
		}
		
		/*
		 * setStationTrack
		 * 
		 * Устанавливает название трека и если оно не пусто, то деактивирует название текущей станции.
		 */
		public static function setStationTrack(name:String):void
		{
			head.station_track.setText(name);
			head.audio_btn.visible = (name.length != 0);
			if (!head.audio_btn.visible)
			{
				head.station_name.stop();
			}
		}
		
		/*
		 * showLoading
		 * 
		 * Отображает окно загрузки.
		 */
		public static function showLoading():void
		{
			SvoeRadioApp.instance().loading_window.visible = true;
		}
		
		/*
		 * hideLoading
		 * 
		 * Скрывает окно загрузки.
		 */
		public static function hideLoading():void
		{
			SvoeRadioApp.instance().loading_window.visible = false;
		}
		
		/*
		 * setSearchListDp
		 * 
		 * Устанавливает список станций во вкладке поиск.
		 */
		public static function setSearchListDp(dp:DataProvider):void
		{
			search.stations_list.dataProvider = dp;
			search.disableManageBtns();
		}
		
		/*
		 * setBookmarksListDp
		 * 
		 * Устанавливает список станций во вкладке избранное.
		 */
		public static function setBookmarksListDp(dp:DataProvider):void
		{
			bookmarks.stations_list.dataProvider = dp;
			bookmarks.disableManageBtns();
		}
		
		/*
		 * setMyStationsDp
		 * 
		 * Устанавливает список станций пользователя во вкладке создать на слайде мои станции.
		 */
		public static function setMyStationsDp(dp:DataProvider):void
		{
			create.station.dataProvider = dp;
		}
		
		/*
		 * addNotice
		 * 
		 * Устанавливает в статусную строку сообщение и делает его временным (если это нужно).
		 */
		public static function addNotice(msg:String, isMoment:Boolean = false):void
		{
			var status:StatusLine = SvoeRadioApp.instance().status_line;
			if (isMoment)
				status.addMomentMessage(msg);
			else status.addMessage(msg);
		}
		
		/*
		 * addMessage
		 * 
		 * Создает всплывающее окно с кнопкок "ОК".
		 */
		public static function addMessage(msg:String):void {
			var app:SvoeRadioApp = SvoeRadioApp.instance();
			var window:MessageWindow = new MessageWindow(app.stage.stageWidth, app.stage.stageHeight);
			window.setMessage(msg);
			app.addChild(window);
		}
		
		/*
		 * addCritical
		 * 
		 * Создает критическое окно.
		 */
		public static function addCritical(msg:String):void
		{
			SvoeRadioApp.instance().loading_window.setError(msg);
			showLoading();
		}
		
		/*
		 * errorHandler
		 * 
		 * Обработчик ошибок: пишет ошибку в статусной строке или в окне загрузки.
		 */
		public static function errorHandler(error:Object):void
		{
			var code:int = int(error.error_code);
			hideLoading();
			switch (code)
			{
				case ERROR_CODE_NOTICE:
					addNotice(error.error_msg);
					setStationTrack(Messages.EMPT_STRING);
					break;
				case ERROR_CODE_MESSAGE:
					addMessage(error.error_msg);
					setStationTrack(Messages.EMPT_STRING);
					break;
				case ERROR_CODE_API: 
					addMessage(error.error_msg);
					break;
				case ERROR_CODE_CRITICAL:
					addMessage(error.error_msg);
					setStationTrack(Messages.EMPT_STRING);
					break;
				case ERROR_CODE_PROVIDER:
					addCritical(error.error_msg);
					break;
				default: 
					var msg:String = "{code:" + code + ",msg:|" + error.error_msg + "|}";
					addCritical(Messages.ERROR_UNKNOWN.replace("%s", msg));
					break;
			}
		}
		
		/*
		 * uncheckCreate
		 * 
		 * После создания станции очищает поля и делает выбор на первом элементе списков.
		 */
		public static function uncheckCreate():void
		{
			create.sname.text = Messages.EMPT_STRING;
			create.genre.selectedIndex = 0;
			create.group.selectedIndex = 0;
		}
		
		/*
		 * onBookmarkBtnClick
		 * 
		 * Обработчик события "При клике на кнопке избранного в голове".
		 * Если текущая станция в закладках, то удалить из закладок, иначе добавить в закладки.
		 */
		private static function onBookmarkBtnClick(e:MouseEvent):void
		{
			updateBookmarkButton(KeepManager.station, true);
			
			search.unSelectStationsList();
			bookmarks.unSelectStationsList();
			onBookmarksTabSearchBtnClick(e);
			
		}
		
		/*
		 * onAudioBtnClick
		 * 
		 * Обработчик события "При клике на кнопку добавления трека в мои аудиозаписи".
		 * Если текущий трек не пуст, добавить его в мои аудиозаписи.
		 */
		private static function onAudioBtnClick(e:MouseEvent):void
		{
			var audio_id:String = KeepManager.audio;
			if (audio_id == null) return;
			IOManager.addAudio(audio_id);
		}
		
		/*
		 * onVolumeChange
		 * 
		 * Обработчик события "При изменении громкости".
		 * Сохраняет громкость и устанавливает в плеере аудио.
		 */
		private static function onVolumeChange(e:VolumeEvent):void
		{
			var volume:Number = e.value;
			KeepManager.volume = volume;
			AudioManager.setVolume(volume);
			addNotice(Messages.NOTICE_VOLUME_CHANGE.replace("%d", int(volume * 100)), true);
		}
		
		/*
		 * onPlayBtnClick
		 * 
		 * Обработчик события "При клике на кнопку играть" и "при двойном клике на элементе списка станций".
		 * Если нажали на кнопку в голове, то грузится следующий трек текущей станции.
		 * Иначе вызывается обработчик onTabPlayBtnClick.
		 */
		private static function onPlayBtnClick(e:MouseEvent):void
		{
			setStationTrack(Messages.EMPT_STRING);
			AudioManager.stop();
			addNotice(Messages.NOTICE_LOAD_STREAM, true);
			if (e.type == MouseEvent.CLICK)
				IOManager.getBroadcast(KeepManager.station);
			else onTabPlayBtnClick(e);
		}
		
		/*
		 * onStopBtnClick
		 * 
		 * Обработчик события "При клике на кнопку стоп в голове".
		 * Останавливает воспроизведение аудио.
		 */
		private static function onStopBtnClick(e:MouseEvent):void
		{
			AudioManager.stop();
			setStationTrack(Messages.EMPT_STRING);
			addNotice(Messages.NOTICE_WELCOME_TO_THE_JUNGLE);
		}
		
		/*
		 * onRatingStarClick
		 * 
		 * Обработчик события "При клике на звезду рейтига".
		 * Отправляет голос текущего значения звезды за текущую станцию.
		 */
		private static function onRatingStarClick(e:RatingEvent):void
		{
			var station_id:int = KeepManager.station;
			var value:int = e.value;
			IOManager.rate(station_id, value);
		}
		
		/*
		 * onBugBtnClick
		 * 
		 * Обработчик события "При клике на кнопку сообщить ошибку".
		 * Открывает страницу обратной связи.
		 */
		private static function onBugBtnClick(e:MouseEvent):void
		{
			IOManager.goURL(Config.FEEDBACK_URL);
		}
		
		/*
		 * onFeedbackBtnClick
		 * 
		 * Обработчик события "При клике на кнопку обратная связь".
		 * Открывает страницу обратной связи.
		 */
		private static function onFeedbackBtnClick(e:MouseEvent):void
		{
			IOManager.goURL(Config.FEEDBACK_URL);
		}
		
		/*
		 * onSearchBtnClick
		 * 
		 * Обработчик события "при клике на кнопку поиск во вкладке поиск".
		 * Производит поиск станций по текущему значению поисковой строки и номера жанра.
		 */
		private static function onSearchBtnClick(e:MouseEvent):void
		{
			var q:String = search.search_input.text,
				genre_id:int = search.genres_box.selectedItem.id;
				
			IOManager.search(q, genre_id);
		}
		
		/*
		 * onBookmarksTabSearchBtnClick
		 * 
		 * Обработчик события "при клике на кнопку поиск во вкладке избранное"
		 * Производит поиск станций по списку избранного по текущему значению поисковой строки и номера жанра.
		 */
		private static function onBookmarksTabSearchBtnClick(e:MouseEvent):void
		{
			var favorite_ids:String = KeepManager.favorite.join(","),
				q:String = bookmarks.search_input.text,
				genre_id:int = bookmarks.genres_box.selectedItem.id;
			IOManager.getStations(favorite_ids, q, genre_id);
		}
		
		/*
		 * onStationListChange
		 * 
		 * Обработчик события "При выборе элемента в списке станций во вкладках поиск и избранное".
		 * Меняет графику в соответствии с данными выбранной станции.
		 */
		private static function onStationListChange(e:Event):void
		{
			var tab:SearchTab = e.currentTarget.parent;
			var selected_id:int = tab.stations_list.selectedItem.id;
			
			tab.play_btn.enabled = true;
			tab.bookmark_btn.enabled = true;
			tab.group_btn.enabled = true;
			
			if (tab.getCode() == 0)
				tab.bookmark_btn.label = KeepManager.isFavorite(selected_id)?'удалить из закладок':'добавить в закладки';
		}
		
		/*
		 * onTabPlayBtnClick
		 * 
		 * Обработчик события "при клике на кнопку играть под списком станций во вкладках поиск и избранное".
		 * Устанавливает графику, сохраняет текущую станцию и начинает воспроизведение.
		 */
		private static function onTabPlayBtnClick(e:MouseEvent):void
		{
			var tab:SearchTab = e.currentTarget.parent;
			var station:Object = tab.stations_list.selectedItem;
			setStationName(station.name);
			setRating(Number(station.rating));
			KeepManager.station = station.id;
			updateBookmarkButton(KeepManager.station);
			IOManager.getBroadcast(station.id);
		}
		
		/*
		 * onTabBookmarkBtnClick
		 * 
		 * Обработчик события "при клике на кнопку избранного во вкладках поиск и избранное".
		 * Если выбранная станция есть в избранном, до удаляется из избранного.
		 * Иначе добавляется в избранное.
		 * Обновляется надпись на кнопке избранного для вкладки поиск.
		 * Вызывается обработчик onBookmarksTabSearchBtnClick
		 */
		private static function onTabBookmarkBtnClick(e:MouseEvent):void
		{
			var station_id:int = e.currentTarget.parent.stations_list.selectedItem.id,
				is_favor:Boolean = KeepManager.isFavorite(station_id);
				
			is_favor ? KeepManager.removeFavorite(station_id) : KeepManager.addFavorite(station_id);
			is_favor = !is_favor;
			
			updateBookmarkButton(station_id);
			
			if (e.currentTarget.parent.getCode() == 0)
				search.bookmark_btn.label = is_favor ? 'удалить из закладок' : 'добавить в закладки';
				
			onBookmarksTabSearchBtnClick(e);
		}
		
		/*
		 * onTabGroupBtnClick
		 * 
		 * Обработчик события "при клике на кнопку группы станции во вкладках поиск и избранное".
		 * Открывает страницу группы станции.
		 */
		private static function onTabGroupBtnClick(e:MouseEvent):void
		{
			var tab:SearchTab = e.currentTarget.parent;
			var group_id:int = tab.stations_list.selectedItem.group_id;
			IOManager.goURL("http://vk.com/club" + group_id);
		}
		
		/*
		 * onCreateBtnClick
		 * 
		 * Обработчик события "при клике на кнопку создания новой станции".
		 * Создает станцию по поляем название, жанр и группа во вкладке создать.
		 */
		private static function onCreateBtnClick(e:MouseEvent):void
		{
			if (create.sname.text.length == 0) {
				addMessage(Messages.MESSAGE_EMPTY_SNAME);
				return;
			}
			if (create.group.selectedItem == null) {
				addMessage(Messages.MESSAGE_NO_GROUPS);
				return;
			}
			var name:String = create.sname.text,
				genre_id:int = create.genre.selectedItem.id,
				group_id:int = create.group.selectedItem.id;
				
			IOManager.create(name, genre_id, group_id);
		}
		
		/*
		 * onPanelBtnClick
		 * 
		 * Обработчик события "при клике на кнопку панели управления".
		 * Открывает панель управления для выбранной станции из списка станций пользователя.
		 */
		private static function onPanelBtnClick(e:MouseEvent):void
		{
			var selected:Object = create.station.selectedItem;
			if (selected.id == 0)
			{
				addMessage(Messages.MESSAGE_SELECT_LIST_STATION);
				create.panel_btn.enabled = false;
				create.created_date.text = Messages.EMPT_STRING;
				return;
			}
			IOManager.goURL(Config.PANEL_URL + selected.group_id);
		}
		
		/*
		 * onSelectStationClick
		 * 
		 * Обработчик события "При клике на кнопку выбора станции".
		 * Активирует кнопку панели управления и указывает дату создания выбранной станции.
		 */
		private static function onSelectStationClick(e:MouseEvent):void
		{
			var selected:Object = create.station.selectedItem;
			if (selected.id == 0)
			{
				addMessage(Messages.MESSAGE_SELECT_LIST_STATION);
				create.panel_btn.enabled = false;
				create.created_date.text = Messages.EMPT_STRING;
				return;
			}
			create.panel_btn.enabled = true;
			create.created_date.text = selected.created;
		}
		
		/*
		 * onCreateOfferGenreBtnClick
		 * 
		 * Показывает окно с информацией по предложению жанра.
		 */
		private static function onCreateOfferGenreBtnClick(e:MouseEvent):void
		{
			addMessage(Messages.MESSAGE_OFFER_GENRE);
		}
	}
}