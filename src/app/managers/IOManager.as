package app.managers
{
	import app.io.ServerAPI;
	import app.misc.Config;
	import app.misc.Messages;
	import fl.data.DataProvider;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import vk.APIConnection;
	
	/**
	 * Обеспечивает взаимодействие с APIs
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	
	public class IOManager
	{
		private static var VK:APIConnection;
		private static var server:ServerAPI;
		
		/*
		 * init
		 * 
		 * Производит инициализацию менеджера.
		 */
		public static function init():void
		{
			VK = new APIConnection(SvoeRadioApp.flashVars);
			server = new ServerAPI(SvoeRadioApp.flashVars);
			VK.forceDirectApiAccess();
			//VK.api("storage.set", { key:"volume", value:"" }, function(e:Object):void { }, onVKErrorHandler);
			//VK.api("storage.set", { key:"station", value:"" }, function(e:Object):void { }, onVKErrorHandler);
			//VK.api("storage.set", { key:"favorite", value:"" }, function(e:Object):void {}, onVKErrorHandler);
		}
		
		/*
		 * firstLoad
		 * 
		 * Производит первый запрос: загрузка первичных данных.
		 */
		public static function firstLoad():void
		{
			UIManager.showLoading();
			VK.api("execute", {code: Config.FIRST_VK_QUERY}, function(api_result:Object):void {
				var i:int = 0;
				var groups:Array = new Array();
				var temp:Array = api_result.groups as Array;
				for (i = 1; i < temp.length; i++)
				{
					groups.push({id: temp[i].gid, label: temp[i].name});
				}
				api_result.groups = new DataProvider(groups);
				
				var station_id:int = int(api_result.station);
				var group_id:int = int(SvoeRadioApp.flashVars.group_id);
				var station_ids:String = String(api_result.favorite);
				server.api("common.firstApp", {station_id: station_id, group_id: group_id, station_ids: station_ids}, function(result:Object):void
				{
					//api_result.genres = result.genres;
					var genres:Array = new Array();
					temp = result.genres as Array;
					for (i = 0; i < temp.length; i++)
					{
						genres.push({id: temp[i].id, label: temp[i].name});
					}
					api_result.genres = new DataProvider(genres);
					api_result.genres2 = api_result.genres.clone();
					api_result.genres2.removeItemAt(0);
					
					api_result.favorite = new DataProvider(result.favorite);
					
					api_result.stations = new DataProvider(result.stations);
					
					var my_stations:Array = new Array();
					temp = result.my as Array;
					my_stations.push({"id": 0, "label": "выберите из списка", "group_id": 0, "created": Messages.EMPT_STRING});
					for (i = 0; i < temp.length; i++)
					{
						my_stations.push({"id": temp[i].id, "label": temp[i].name, "group_id": temp[i].group_id, "created": temp[i].created_date});
					}
					api_result.my_stations = new DataProvider(my_stations);
					
					api_result.station = result.station;
					
					api_result.isOwner = result.isOwner;
					api_result.station_ids = station_ids;
					api_result.volume = ((api_result.volume <= 0) || (api_result.volume > 1)) ? Config.DEFAULT_VOLUME : api_result.volume;
					
					if (int(result.isGroup) == 1)
						getBroadcast(api_result.station.id);
						
					UIManager.firstLoad(api_result);
					AudioManager.setVolume(Number(api_result.volume));
					KeepManager.first(api_result);
					UIManager.addNotice(Messages.NOTICE_WELCOME_TO_THE_JUNGLE);
					UIManager.hideLoading();
				}, onErrorHandler);
			}, onVKErrorHandler);
		}
		
		/*
		 * goURL
		 * 
		 * Производит переход по ссылке.
		 */
		public static function goURL(url:String):void
		{
			navigateToURL(new URLRequest(url), "_blank");
		}
		
		/*
		 * addAudio
		 * 
		 * Добавляет аудио в мои аудиозаписи пользователя
		 */
		public static function addAudio(aid:String):void
		{
			var temp:Array = aid.split("_");
			var owner_id:int = temp[0];
			var audio_id:int = temp[1];
			UIManager.showLoading();
			VK.api("audio.add", {owner_id: owner_id, audio_id: audio_id}, function(result:Object):void
			{
				UIManager.addMessage(Messages.MESSAGE_TRACK_ADDED);
				UIManager.hideLoading();
			}, onVKErrorHandler);
		}
		
		/*
		 * getBroadcast
		 * 
		 * Получает текущий трек станции.
		 */
		public static function getBroadcast(station_id:int):void
		{
			UIManager.showLoading();
			server.api("queues.getBroadcast", {station_id: station_id}, function(result:Object):void
			{
				VK.api("audio.getById", {audios: result.audio_id}, function(result2:Object):void
				{
					if ((result2 == null) || (result2[0] == null)) {
						onErrorHandler({error_code:UIManager.ERROR_CODE_MESSAGE, error_msg:Messages.MESSAGE_TRACK_DELETED});
						return;
					}
					result2 = result2[0];
					result2.audio_id = result.audio_id;
					result2.time = result.time;
					UIManager.setStationTrack(result2.artist + " - " + result2.title);
					UIManager.addNotice(Messages.NOTICE_BUFFERING);
					AudioManager.play(result2);
					KeepManager.audio = result2.audio_id;
					UIManager.hideLoading();
				}, onVKErrorHandler);
			}, onErrorHandler);
		}
		
		/*
		 * rate
		 * 
		 * Отправляет голос за выбранную станцию.
		 */
		public static function rate(station_id:int, value:int):void
		{
			UIManager.showLoading();
			server.api("rating.rate", {station_id: station_id, value: value}, function(result:Object):void
			{
				trace("rating", result);
				UIManager.setRating(Number(result));
				UIManager.hideLoading();
			}, onErrorHandler);
		}
		
		/*
		 * search
		 * 
		 * Производит поиск станций для вкладки поиск.
		 */
		public static function search(q:String, genre_id:int):void
		{
			UIManager.showLoading();
			server.api("stations.search", {q: q, genre_id: genre_id}, function(result:Object):void
			{
				UIManager.setSearchListDp(new DataProvider(result));
				UIManager.hideLoading();
			}, onErrorHandler);
		}
		
		/*
		 * getStations
		 * 
		 * Производит поиск станций для вкладки избранное.
		 */
		public static function getStations(ids:String, q:String, genre_id:int):void
		{
			UIManager.showLoading();
			server.api("stations.search", {q: q, genre_id: genre_id, station_ids: ids}, function(result:Object):void
			{
				UIManager.setBookmarksListDp(new DataProvider(result));
				UIManager.hideLoading();
			}, onErrorHandler);
		}
		
		/*
		 * create
		 * 
		 * Создает новую станцию пользователя.
		 */
		public static function create(name:String, genre_id:int, group_id:int):void
		{
			UIManager.showLoading();
			server.api("stations.add", {name: name, genre_id: genre_id, group_id: group_id}, function(result:Object):void
			{
				UIManager.addMessage(Messages.MESSAGE_STATION_SUCC_CREAT);
				UIManager.uncheckCreate();
				//UIManager.hideLoading(); это будет в updateMyStations
				updateMyStations();
			}, onErrorHandler);
		}
		
		/*
		 * updateMyStations
		 * 
		 * Производит обновления списка станций пользователя для вкладки создать.
		 */
		public static function updateMyStations():void
		{
			UIManager.showLoading();
			server.api("stations.getMy", {}, function(result:Object):void
			{
				var stations:DataProvider = new DataProvider();
				stations.addItem({"id": 0, "label": "выберите из списка", "group_id": 0, "created": Messages.EMPT_STRING});
				for (var i:int = 0; i < result.length; i++)
				{
					stations.addItem({"id": result[i].id, "label": result[i].name, "group_id": result[i].group_id, "created": result[i].created_date});
				}
				UIManager.setMyStationsDp(stations);
				UIManager.hideLoading();
			}, onErrorHandler);
		}
		
		/*
		 * saveVar
		 * 
		 * Сохраняет переменную на серверах ВК.
		 */
		public static function saveVar(key:String, value:String):void
		{
			VK.api("storage.set", {key: key, value: value}, function(result:Object):void {}, onVKErrorHandler);
		}
		
		/*
		 * onVKErrorHandler
		 * 
		 * Обработчик ошибок от VK API.
		 */
		private static function onVKErrorHandler(result:Object):void
		{
			if (typeof(result) == "string") {
				result = { error_code: UIManager.ERROR_CODE_PROVIDER, error_msg: (result as String) };
			}
			onErrorHandler(result);
		}
		
		/*
		 * onErrorHandler
		 * 
		 * Обработчик ошибок.
		 */
		private static function onErrorHandler(error:Object):void
		{
			if (!error.error_code) {
				error = {
					error_code: UIManager.ERROR_CODE_PROVIDER,
					error_msg: (error as String)
				};
			}
			UIManager.errorHandler(error);
		}
	}
}