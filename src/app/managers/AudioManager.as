package app.managers
{
	import app.misc.Messages;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	/**
	 * Управляет аудиопотоком.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	
	public class AudioManager
	{
		private static var time:int; // для буферизации
		private static var buffering:Boolean; // для буферизации
		private static var buff_time:Number; // для буферизации
		private static var sound:Sound;
		private static var sound_transform:SoundTransform;
		private static var sound_channel:SoundChannel;
		private static var broadcast_timer:Timer; // таймер для автопереключения треков
		
		/*
		 * init
		 * 
		 * Производит инициализацию менеджера.
		 */
		public static function init():void
		{
			sound_transform = new SoundTransform();
			sound_channel = new SoundChannel();
			
			broadcast_timer = new Timer(1000, 1);
			broadcast_timer.addEventListener(TimerEvent.TIMER_COMPLETE, function(e:TimerEvent):void {
				IOManager.getBroadcast(KeepManager.station);
			});
		}
		
		// установка громкости воспроизведения
		public static function setVolume(value:Number):void
		{
			sound_transform.volume = value;
			sound_channel.soundTransform = sound_transform;
		}
		
		// остановка воспроизведения
		public static function stop():void
		{
			if (sound_channel != null)
			{
				sound_channel.stop();
			}
			
			if (sound != null)
			{
				try
				{
					sound.close();
				}
				catch (e:IOError)
				{
				}
			}
			KeepManager.audio = Messages.EMPT_STRING;
			broadcast_timer.stop();
		}
		
		// воспроизведение
		public static function play(track:Object):void
		{
			if (sound_channel != null)
			{
				sound_channel.stop();
			}
			
			if (sound != null)
			{
				try
				{
					sound.close();
				}
				catch (e:IOError)
				{
				}
			}
			
			time = track.time * 1000;
			buffering = true;
			buff_time = new Date().time;
			
			sound = new Sound(new URLRequest(track.url));
			sound.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):void {
				stop();
				UIManager.errorHandler( { error_msg: Messages.ERROR_AUDIO_IO, error_code: UIManager.ERROR_CODE_PROVIDER } );
				UIManager.setStationTrack(Messages.EMPT_STRING);
			});
			sound.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
			sound_channel = sound.play(time, 0, sound_transform);
			sound_channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleteHandler);
			
			broadcast_timer.stop();
			broadcast_timer.delay = (track.duration - track.time) * 1000;
			broadcast_timer.start();
		}
		
		/*
		 * onProgressHandler
		 * 
		 * Обработчик события "когда производится загрузка аудио".
		 * Проверяет достаточно ли заполнен буфер и можно ли воспроизводить его.
		 */
		private static function onProgressHandler(e:ProgressEvent):void
		{
			if ((sound.length >= time) && buffering)
			{
				sound.removeEventListener(ProgressEvent.PROGRESS, onProgressHandler);
				sound_channel.stop();
				sound_channel = sound.play(time, 0, sound_transform);
				sound_channel.addEventListener(Event.SOUND_COMPLETE, onSoundCompleteHandler);
				time = 0;
				buffering = false;
				UIManager.addNotice(Messages.NOTICE_PLAYING);
				UIManager.playStationName();
			}
			else
			{
				var now:Date = new Date();
				var diff_time:int = int(now.time - buff_time);
				time += diff_time;
				buff_time = now.time;
			}
		}
		
		/*
		 * onSoundCompleteHandler
		 * 
		 * Обработчик события "когда текущий трек закончит воспроизведение".
		 * Очищает текущий трек в голове.
		 */
		private static function onSoundCompleteHandler(e:Event):void
		{
			UIManager.setStationTrack(Messages.EMPT_STRING);
			UIManager.addNotice(Messages.NOTICE_TRACK_END);
		}
	}
}