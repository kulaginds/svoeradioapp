package app.managers
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * Хранит параметры.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	
	public class KeepManager
	{
		private static var station_id:int; // текущая станция
		private static var favorite_ids:Array; // избранные станции
		private static var vol:Number; // громкость
		private static var audio_id:String; // текущий трек
		
		private static var volume_timer:Timer; // таймер для автосохранения громкости
		
		/*
		 * init
		 * 
		 * Производит инициализацию менеджера.
		 */
		public static function init():void
		{
			volume_timer = new Timer(3000);
			volume_timer.addEventListener(TimerEvent.TIMER, function(e:TimerEvent):void {
				IOManager.saveVar("volume", volume.toString());
				volume_timer.stop();
			});
		}
		
		/*
		 * first
		 * 
		 * Метод первичной загрузки приложения.
		 * Сохраняет текущую станцию (если она есть), громкость, список избранного.
		 */
		public static function first(data:Object):void
		{
			station_id = data.station.id;
			vol = data.volume;
			favorite = String(data.station_ids).split(",");
			UIManager.updateBookmarkButton(station_id);
		}
		
		// получение текущей станции
		public static function get station():int
		{
			return station_id;
		}
		
		// установка текущей станции
		public static function set station(value:int):void
		{
			if (station_id == value) return;
			station_id = value;
			IOManager.saveVar("station", value.toString());
		}
		
		// получение списка избранного
		public static function get favorite():Array
		{
			return favorite_ids;
		}
		
		// установка списка избранного
		public static function set favorite(value:Array):void
		{
			favorite_ids = new Array();
			for (var i:int = 0; i < value.length; i++) {
				var id:int = parseInt(value[i]) | 0;
				if (isFavorite(value[i]))
					continue;
				if (id > 0)
					favorite_ids.push(id);
			}
			saveFavorite();
		}
		
		// проверка, является ли станция избранной
		public static function isFavorite(value:int):Boolean
		{
			for (var i:int = 0; i < favorite_ids.length; i++)
			{
				if (value == favorite_ids[i])
					return true;
			}
			return false;
		}
		
		// удаляет станцию из избранного если она там есть
		public static function removeFavorite(value:int):void
		{
			var temp:Array = new Array();
			for (var i:int = 0; i < favorite_ids.length; i++)
			{
				if (favorite_ids[i] != value) temp.push(favorite_ids[i]);
			}
			favorite_ids = temp;
			saveFavorite();
		}
		
		// добавляет станцию в избранное если её там нет
		public static function addFavorite(value:int):void
		{
			if (!isFavorite(value)) {
				favorite_ids.push(value);
				saveFavorite();
			}
		}
		
		// сохраняет избранные станции
		public static function saveFavorite():void
		{
			IOManager.saveVar("favorite", favorite_ids.join(","));
		}
		
		// получение громкости
		public static function get volume():Number
		{
			return vol;
		}
		
		// установка громкости
		public static function set volume(value:Number):void
		{
			vol = value;
			volume_timer.stop();
			volume_timer.start();
		}
		
		// получения текущего аудио
		public static function get audio():String
		{
			return audio_id;
		}
		
		// установка текущего аудио
		public static function set audio(value:String):void
		{
			audio_id = value;
		}
	}
}