package app.ui.tabs
{
	import flash.display.Sprite;
	
	/**
	 * Контейнер с вкладками
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Tabs extends Sprite
	{
		protected var containers:Array;
		protected var buttons:Array;
		
		protected var buttons_height:int = 60;
		protected var tabs_height:int;
		protected var buttons_x:int;
		
		public function Tabs(width:int, height:int)
		{
			super();
			
			tabs_height = height - buttons_height;
			
			graphics.beginFill(0xFFFFFF, 1);
			graphics.lineTo(0, 0);
			graphics.moveTo(width, 0);
			graphics.moveTo(width, tabs_height);
			graphics.moveTo(0, tabs_height);
			graphics.lineStyle(1, 0xDDDDDD);
			graphics.drawRect(0, 0, width, tabs_height);
			graphics.endFill();
			
			containers = new Array();
			buttons = new Array();
		}
		
		public function addTab(button:TabButton, tab:Sprite):int
		{
			tab.x = tab.y = 0;
			tab.visible = false;
			addChild(tab);
			
			button.x = buttons_x - (containers.length + 1);
			button.y = tabs_height;
			button.addEventListener(TabEvent.CHANGE, onTabChange);
			addChild(button);
			
			buttons_x += button.width;
			
			containers.push(tab);
			buttons.push(button);
			
			return (containers.length - 1);
		}
		
		public function openTab(index:int):void
		{
			if (containers.length <= index)
				return;
			hiddenAll();
			containers[index].visible = true;
			buttons[index].setState(1);
		}
		
		protected function hiddenAll():void
		{
			for (var i:int = 0; i < containers.length; i++)
			{
				containers[i].visible = false;
				buttons[i].setState(0);
			}
		}
		
		protected function onTabChange(e:TabEvent):void
		{
			openTab(e.index);
		}
	
	}

}