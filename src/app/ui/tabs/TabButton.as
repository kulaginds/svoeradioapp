package app.ui.tabs
{
	import app.ui.buttons.ToggleIconButton;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	
	/**
	 * Кнопка переключения на вкладку
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class TabButton extends ToggleIconButton
	{
		protected var val:int;
		
		public function TabButton(icon1:BitmapData, icon2:BitmapData)
		{
			super(icon1, icon2);
		}
		
		public function set value(i:int):void
		{
			val = i;
		}
		
		public function get value():int
		{
			return val;
		}
		
		override protected function onClick(e:MouseEvent):void
		{
			dispatchEvent(new TabEvent(TabEvent.CHANGE, val));
		}
	
	}

}