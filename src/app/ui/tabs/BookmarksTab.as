package app.ui.tabs
{
	import fl.controls.DataGrid;
	
	/**
	 * Вкладка закладки.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class BookmarksTab extends SearchTab
	{
		
		public function BookmarksTab()
		{
			super();
			bookmark_btn.label = 'удалить из закладок';
		}
		
		override public function enableButtons():void
		{
			bookmark_btn.enabled = true;
			search_btn.enabled = true;
			play_btn.enabled = true;
			group_btn.enabled = true;
			
			updateButtonsStyles();
		}
		
		public function getBookmarks():DataGrid
		{
			return super.stations_list;
		}
	}
}