package app.ui.tabs
{
	
	/**
	 * Интерфейс контейнера вкладки
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public interface ITabContainer
	{
		function getCode():int;
		function setCode(code:int):void;
	}

}