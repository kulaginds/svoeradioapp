package app.ui.tabs
{
	import app.misc.Config;
	import app.ui.buttons.Button;
	import app.ui.combobox.ComboBox;
	import app.ui.datagrid.DataGrid;
	import app.ui.text.TextInput;
	import fl.controls.dataGridClasses.DataGridColumn;
	import flash.display.Sprite;
	
	/**
	 * Вкладка поиск.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class SearchTab extends Sprite implements ITabContainer
	{
		protected var padding:int = 10;
		protected var code:int;
		
		public var search_input:TextInput;
		public var genres_box:ComboBox;
		public var search_btn:Button;
		public var stations_list:DataGrid;
		public var play_btn:Button;
		public var bookmark_btn:Button;
		public var group_btn:Button;
		
		public function SearchTab()
		{
			super();
			
			// Поисковая строка.
			search_input = new TextInput();
			search_input.width = 250;
			search_input.x = padding;
			search_input.y = padding;
			addChild(search_input);
			
			// Выпадающий список с жанрами.
			genres_box = new ComboBox();
			genres_box.width = 110;
			genres_box.x = search_input.x + search_input.width + padding;
			genres_box.y = search_input.y;
			addChild(genres_box);
			
			// Кнопка поиска.
			search_btn = new Button();
			search_btn.label = 'поиск';
			search_btn.x = genres_box.x + genres_box.width + padding;
			search_btn.y = search_input.y;
			search_btn.width = 60;
			addChild(search_btn);
			
			stations_list = new DataGrid();
			stations_list.width = 440;
			stations_list.height = 164;
			stations_list.x = padding;
			stations_list.y = search_input.y + search_input.height + padding;
			
			var cols:Array = new Array();
			
			var name_col:DataGridColumn = new DataGridColumn();
			name_col.dataField = 'name';
			name_col.headerText = 'Название станции'
			name_col.width = 280;
			name_col.resizable = false;
			cols.push(name_col);
			
			var genre_col:DataGridColumn = new DataGridColumn();
			genre_col.dataField = 'genre_name';
			genre_col.headerText = 'Жанр'
			genre_col.width = 80;
			genre_col.resizable = false;
			cols.push(genre_col);
			
			stations_list.columns = cols;
			addChild(stations_list);
			
			play_btn = new Button();
			play_btn.enabled = false;
			play_btn.label = 'играть';
			play_btn.x = 114;
			play_btn.y = stations_list.height + stations_list.y + padding;
			play_btn.width = 47;
			addChild(play_btn);
			
			bookmark_btn = new Button();
			bookmark_btn.enabled = false;
			bookmark_btn.label = 'добавить в закладки';
			bookmark_btn.x = play_btn.x + play_btn.width + padding;
			bookmark_btn.y = stations_list.height + stations_list.y + padding;
			bookmark_btn.width = 120;
			addChild(bookmark_btn);
			
			group_btn = new Button();
			group_btn.enabled = false;
			group_btn.label = 'перейти в группу станции';
			group_btn.x = bookmark_btn.x + bookmark_btn.width + padding;
			group_btn.y = stations_list.height + stations_list.y + padding;
			group_btn.width = 150;
			addChild(group_btn);
		}
		
		public function unSelectStationsList():void
		{
			stations_list.selectedIndex = -1;
			disableManageBtns();
		}
		
		public function disableBookmarkBtn():void
		{
			bookmark_btn.enabled = false;
			
			updateButtonsStyles();
		}
		
		public function disableManageBtns():void
		{
			play_btn.enabled = false;
			bookmark_btn.enabled = false;
			group_btn.enabled = false;
			
			updateButtonsStyles();
		}
		
		public function enableButtons():void
		{
			search_btn.enabled = true;
			play_btn.enabled = true;
			group_btn.enabled = true;
			
			updateButtonsStyles();
		}
		
		public function setBookmarkBtnEnabled(e:Boolean):void
		{
			bookmark_btn.enabled = e;
		}
		
		public function setCode(code:int):void
		{
			this.code = code;
		}
		
		public function getCode():int
		{
			return code;
		}
		
		protected function updateButtonsStyles():void
		{
			play_btn.setStyle('textFormat', Config.FLEX_TF.clone());
			bookmark_btn.setStyle('textFormat', Config.FLEX_TF.clone());
			group_btn.setStyle('textFormat', Config.FLEX_TF.clone());
		}
	}
}