package app.ui.tabs
{
	import app.misc.Config;
	import app.ui.buttons.Button;
	import app.ui.combobox.ComboBox;
	import app.ui.text.TextInput;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * Вкладка создать.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class CreateTab extends Sprite implements ITabContainer
	{
		protected var padding:int = 10;
		protected var code:int;
		
		public var sname:TextInput;
		public var genre:ComboBox;
		public var offer_genre_btn:Button;
		public var group:ComboBox;
		public var station:ComboBox;
		public var select_btn:Button;
		public var create_btn:Button;
		public var panel_btn:Button;
		
		public var to_create_btn:Button;
		public var to_manage_btn:Button;
		
		public var created_date:TextField;
		
		private var tf:TextFormat;
		private var txt:TextField;
		private var slide1:Sprite = new Sprite();
		private var slide2:Sprite = new Sprite();
		private var selected_slide:int;
		
		public function CreateTab(slide:int = 0)
		{
			super();
			
			selected_slide = slide;
			
			slide1.y = 60;
			slide2.y = 60;
			
			to_create_btn = new Button();
			to_create_btn.label = 'Создать станцию';
			to_create_btn.width = 105;
			to_create_btn.x = 114;
			to_create_btn.y = 25;
			to_create_btn.addEventListener(MouseEvent.CLICK, switchSlide);
			addChild(to_create_btn);
			
			to_manage_btn = new Button();
			to_manage_btn.label = 'Мои станции';
			to_manage_btn.width = 80;
			to_manage_btn.x = to_create_btn.x + to_create_btn.width + padding;
			to_manage_btn.y = to_create_btn.y;
			to_manage_btn.addEventListener(MouseEvent.CLICK, switchSlide);
			addChild(to_manage_btn);
			
			// Первый слайд
			
			var tf:TextFormat = Config.FLEX_TF.clone();
			tf.align = TextFormatAlign.RIGHT;
			
			txt = new TextField();
			txt.defaultTextFormat = tf;
			txt.selectable = false;
			txt.text = 'Название:';
			txt.x = to_create_btn.x - txt.width;
			slide1.addChild(txt);
			
			sname = new TextInput();
			sname.x = to_create_btn.x;
			sname.y = 0;
			sname.width = 150;
			sname.maxChars = 15;
			slide1.addChild(sname);
			
			txt = new TextField();
			txt.defaultTextFormat = tf;
			txt.selectable = false;
			txt.text = 'Жанр:';
			txt.x = to_create_btn.x - txt.width;
			txt.y = sname.y + sname.height + padding;
			slide1.addChild(txt);
			
			genre = new ComboBox();
			genre.x = to_create_btn.x;
			genre.y = txt.y;
			slide1.addChild(genre);
			
			offer_genre_btn = new Button();
			offer_genre_btn.label = 'Предложить жанр';
			offer_genre_btn.x = genre.x + genre.width + 10;
			offer_genre_btn.y = genre.y;
			offer_genre_btn.width = offer_genre_btn.width + 10;
			slide1.addChild(offer_genre_btn);
			
			txt = new TextField();
			txt.defaultTextFormat = tf;
			txt.selectable = false;
			txt.text = 'Группа:';
			txt.x = to_create_btn.x - txt.width;
			txt.y = genre.y + genre.height + padding;
			slide1.addChild(txt);
			
			group = new ComboBox();
			group.x = to_create_btn.x;
			group.y = txt.y;
			slide1.addChild(group);
			
			txt = new TextField();
			txt.defaultTextFormat = Config.FLEX_TF.clone();
			txt.selectable = false;
			txt.htmlText = 'Нажимая кнопку "создать" вы соглашаетесь с <a href="' + Config.RULES_URL + '" target="_blank"><font color="#00ADEE">правилами создания станций</font></a>.';
			txt.wordWrap = true;
			txt.width = 340;
			txt.height = 32;
			txt.x = sname.x - 2;
			txt.y = group.y + group.height + padding;
			slide1.addChild(txt);
			
			create_btn = new Button();
			create_btn.label = 'создать';
			create_btn.width = 55;
			create_btn.x = to_create_btn.x;
			create_btn.y = txt.y + txt.height + padding;
			slide1.addChild(create_btn);
			
			// Второй слайд
			
			txt = new TextField();
			txt.defaultTextFormat = tf;
			txt.selectable = false;
			txt.text = 'Выберите станцию для работы.';
			txt.width = 180;
			txt.height = 20;
			txt.x = 100;
			slide2.addChild(txt);
			
			station = new ComboBox();
			station.width = 150;
			station.x = 85;
			station.y = txt.y + txt.height + padding;
			slide2.addChild(station);
			
			select_btn = new Button();
			select_btn.label = 'Выбор станции';
			select_btn.width = 100;
			select_btn.x = station.x + station.width + padding;
			select_btn.y = station.y;
			slide2.addChild(select_btn);
			
			txt = new TextField();
			txt.defaultTextFormat = tf;
			txt.selectable = false;
			txt.text = 'Дата создания:';
			txt.height = 20;
			txt.x = station.x;
			txt.y = station.y + station.height + padding;
			slide2.addChild(txt);
			
			created_date = new TextField();
			created_date.defaultTextFormat = tf;
			created_date.selectable = false;
			created_date.text = '';
			created_date.width = 105;
			created_date.x = txt.x + txt.width;
			created_date.y = txt.y;
			slide2.addChild(created_date);
			
			panel_btn = new Button();
			panel_btn.label = 'Управлять';
			panel_btn.width = 70;
			panel_btn.x = station.x;
			panel_btn.y = txt.y + txt.height + padding;
			panel_btn.enabled = false;
			slide2.addChild(panel_btn);
			
			addChild(slide1);
			addChild(slide2);
			
			redraw();
		}
		
		private function switchSlide(e:MouseEvent):void
		{
			if (selected_slide == 0)
				setSlide(1);
			else
				setSlide(0);
		}
		
		public function setSlide(slide:int = 0):void
		{
			selected_slide = ((slide == 0) || (slide == 1)) ? slide : selected_slide;
			redraw();
		}
		
		public function setCode(code:int):void
		{
			this.code = code;
		}
		
		public function getCode():int
		{
			return code;
		}
		
		private function redraw():void
		{
			slide1.visible = false;
			slide2.visible = false;
			to_manage_btn.enabled = true;
			to_create_btn.enabled = true;
			
			switch (selected_slide)
			{
			case 0: 
				slide1.visible = true;
				to_create_btn.enabled = false;
				break;
			case 1: 
				slide2.visible = true;
				to_manage_btn.enabled = false;
				break;
			}
		}
	}
}