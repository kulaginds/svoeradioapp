package app.ui.tabs
{
	import flash.events.Event;
	
	/**
	 * События компонента Menu.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class TabEvent extends Event
	{
		public static const CHANGE:String = 'tab change';
		
		private var ind:int;
		
		public function TabEvent(type:String, index:int)
		{
			super(type);
			ind = index;
		}
		
		public function get index():int
		{
			return ind;
		}
		
		override public function clone():Event
		{
			return new TabEvent(type, ind);
		}
	}
}