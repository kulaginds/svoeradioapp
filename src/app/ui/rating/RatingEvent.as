package app.ui.rating
{
	import flash.events.Event;
	
	/**
	 * Событие звезды.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class RatingEvent extends Event
	{
		public static const CLICK:String = "star click";
		
		private var _value:int;
		
		public function RatingEvent(type:String, value:int)
		{
			super(type);
			_value = value;
		}
		
		public function get value():int
		{
			return _value;
		}
		
		override public function clone():Event
		{
			return new RatingEvent(type, value);
		}
	
	}

}