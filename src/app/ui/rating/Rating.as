package app.ui.rating
{
	import app.ui.text.TF;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	
	/**
	 * Рейтинг текущей станции.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Rating extends Sprite
	{
		protected var debug:Boolean;
		
		private var group:RatingStarsGroup;
		
		public function Rating(debug:Boolean)
		{
			super();
			
			this.debug = debug;
			
			group = new RatingStarsGroup(5);
			addChild(group);
			
			group.addEventListener(RatingEvent.CLICK, onRatingChange);
			
			if (debug)
				setRating(4.897);
		}
		
		public function setRating(value:Number):void
		{
			group.value = int(Math.floor(value));
			SvoeRadioApp.instance().tt.Register(group, value);
		}
		
		protected function onRatingChange(e:RatingEvent):void
		{
			dispatchEvent(e);
			debug ? trace("rating", e.value) : null;
		}
	}
}