package app.ui.rating
{
	import flash.display.Sprite;
	
	/**
	 * Группа звезд
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class RatingStarsGroup extends Sprite
	{
		protected var padding:int = 2;
		protected var count:int;
		protected var list:RatingStar;
		protected var val:int;
		
		public function RatingStarsGroup(count:int)
		{
			this.count = count;
			var star:RatingStar;
			for (var i:int = 0; i < count; i++)
			{
				star = new RatingStar(i + 1);
				addChild(star);
				star.addEventListener(RatingEvent.CLICK, onStarClick);
				star.x = i * (padding + star.width);
				star.next = list;
				list = star;
			}
		}
		
		public function set value(v:int):void
		{
			if ((v > count) || (v < 0))
				return;
			val = v;
			var item:RatingStar = list;
			while (item != null)
			{
				item.updateValue(v);
				item = item.next;
			}
		}
		
		public function get value():int
		{
			return val;
		}
		
		protected function onStarClick(e:RatingEvent):void
		{
			val = e.value;
			dispatchEvent(e);
		}
	
	}

}