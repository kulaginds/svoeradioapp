package app.ui.rating
{
	import app.misc.Import;
	import app.ui.buttons.ToggleIconButton;
	import flash.events.MouseEvent;
	
	/**
	 * Звезда рейтинга.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class RatingStar extends ToggleIconButton
	{
		protected var val:int;
		
		public var next:RatingStar;
		
		public function RatingStar(value:int)
		{
			super(Import.rating_deactive_star, Import.rating_active_star);
			val = value;
		}
		
		public function updateValue(v:int):void
		{
			setState((val <= v) ? ToggleIconButton.ACTIVE : ToggleIconButton.PASSIVE);
		}
		
		override protected function onClick(e:MouseEvent):void
		{
			dispatchEvent(new RatingEvent(RatingEvent.CLICK, val));
		}
	}
}