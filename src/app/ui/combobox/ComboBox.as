package app.ui.combobox
{
	import app.misc.Config;
	import fl.controls.ComboBox;
	
	/**
	 * Выпадающий список
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class ComboBox extends fl.controls.ComboBox
	{
		
		public function ComboBox()
		{
			super();
			width = 150;
			setStyle('textFormat', Config.FLEX_TF.clone());
			dropdown.setStyle('cellRenderer', GenresCellRenderer);
			dropdown.setRendererStyle('textFormat', Config.FLEX_TF.clone());
		}
	
	}

}