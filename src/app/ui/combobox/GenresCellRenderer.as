package app.ui.combobox
{
	import fl.controls.listClasses.CellRenderer;
	
	/**
	 * ...
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class GenresCellRenderer extends CellRenderer
	{
		
		public function GenresCellRenderer()
		{
			super();
			
			setStyle('disabledSkin', cb_disabledSkin);
			setStyle('downSkin', cb_downSkin);
			setStyle('overSkin', cb_overSkin);
			setStyle('upSkin', cb_upSkin);
			setStyle('selectedDisabledSkin', cb_selectedDisabledSkin);
			setStyle('selectedDownSkin', cb_selectedDownSkin);
			setStyle('selectedOverSkin', cb_selectedOverSkin);
			setStyle('selectedUpSkin', cb_selectedUpSkin);
		}
	}
}