package app.ui.volume
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * Регулятор громкости.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Volume extends Sprite
	{
		protected var debug:Boolean;
		protected var padding:int = 8; // отступ между кнопкой и слайдером
		protected var vb:VolumeButton; // кнопка Mute
		protected var vs:VolumeSlider; // слайдер
		protected var temp_val:Number; // значение громкости при клике на Mute
		protected var is_mute:Boolean; // отключен ли звук
		
		public function Volume(width:int, debug:Boolean)
		{
			this.debug = debug;
			
			vb = new VolumeButton();
			addChild(vb);
			
			vs = new VolumeSlider(width - vb.x - vb.width - padding);
			vs.x = vb.x + vb.width + padding;
			addChild(vs);
		}
		
		public function init():void
		{
			vs.init();
			vs.addEventListener(VolumeEvent.CHANGE, onVolumeChange);
			vb.addEventListener(MouseEvent.CLICK, onVolumeButtonClick);
		}
		
		public function set value(v:Number):void
		{
			vs.value = v;
		}
		
		public function get value():Number
		{
			return vs.value;
		}
		
		protected function onVolumeButtonClick(e:MouseEvent):void
		{
			is_mute = !is_mute;
			
			if (is_mute)
			{
				temp_val = vs.value;
				vs.value = 0;
			}
			else
			{
				vs.value = temp_val;
				temp_val = 0;
			}
		}
		
		protected function onVolumeChange(e:VolumeEvent):void
		{
			dispatchEvent(e);
			debug ? trace("volume", e.value) : null;
		}
	}
}