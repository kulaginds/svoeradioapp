package app.ui.volume
{
	import app.misc.Import;
	import app.ui.buttons.IconButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	/**
	 * Регулятор
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class VolumeSlider extends Sprite
	{
		protected var line:Sprite; // линия ползунка
		protected var runner:Sprite; // ползунок
		protected var rect:Rectangle; // прямоугольник, в пределах которого будет двигаться ползунок
		protected var val:Number; // значение громкости
		
		public function VolumeSlider(width:int)
		{
			line = new Sprite();
			line.graphics.beginFill(0x00ADEE);
			line.graphics.drawRoundRect(0, 6, width, 5, 5, 5);
			line.graphics.endFill();
			addChild(line);
			
			runner = new Sprite();
			runner.buttonMode = true;
			runner.useHandCursor = true;
			runner.addChild(new IconButton(Import.vol_runner));
			addChild(runner);
			
			rect = new Rectangle(0, 0, (line.width - runner.width), 0);
		}
		
		public function init():void
		{
			runner.addEventListener(MouseEvent.MOUSE_DOWN, onRunnerMouseDown);
			root.stage.addEventListener(MouseEvent.MOUSE_UP, onRunnerMouseUp);
		}
		
		public function set value(v:Number):void
		{
			val = ((v >= 0) || (v <= 1)) ? v : val;
			runner.x = val * rect.width;
			dispatchEvent(new VolumeEvent(VolumeEvent.CHANGE, val));
		}
		
		public function get value():Number
		{
			return val;
		}
		
		protected function onRunnerMouseDown(e:MouseEvent):void
		{
			runner.startDrag(false, rect);
			addEventListener(MouseEvent.MOUSE_MOVE, onRunnerMove);
		}
		
		protected function onRunnerMouseUp(e:MouseEvent):void
		{
			runner.stopDrag();
			removeEventListener(MouseEvent.MOUSE_MOVE, onRunnerMove);
		}
		
		protected function onRunnerMove(e:MouseEvent):void
		{
			val = runner.x / rect.width;
			dispatchEvent(new VolumeEvent(VolumeEvent.CHANGE, val));
		}
	
	}

}