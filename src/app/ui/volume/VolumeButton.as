package app.ui.volume
{
	import app.misc.Import;
	import app.ui.buttons.ToggleIconButton;
	
	/**
	 * VolumeButton
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class VolumeButton extends ToggleIconButton
	{
		public function VolumeButton()
		{
			super(Import.sound_on, Import.sound_off);
		}
	}
}