package app.ui.volume
{
	import flash.events.Event;
	
	/**
	 * События компонента Volume.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class VolumeEvent extends Event
	{
		public static const CHANGE:String = 'volume change';
		
		private var _value:Number;
		
		public function VolumeEvent(type:String, value:Number)
		{
			super(type);
			_value = value;
		}
		
		public function get value():Number
		{
			return _value;
		}
		
		override public function clone():Event 
		{
			return new VolumeEvent(type, value);
		}
	}
}