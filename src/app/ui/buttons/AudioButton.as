package app.ui.buttons
{
	import app.misc.Import;
	import app.ui.buttons.IconButton;
	
	/**
	 * Кнопка добавления трека в аудиозаписи.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class AudioButton extends IconButton
	{
		
		public function AudioButton()
		{
			super(Import.add_audio);
		}
	}
}