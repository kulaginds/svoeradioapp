package app.ui.buttons
{
	import app.misc.Import;
	
	/**
	 * Кнопка сообщения багов.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class BugButton extends IconButton
	{
		
		public function BugButton()
		{
			super(Import.bug_btn);
		}
	
	}

}