package app.ui.buttons
{
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	
	/**
	 * Кнопка-иконка с нажатым и отжатым состояниями
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class ToggleIconButton extends IconButton
	{
		public static const PASSIVE:int = 0;
		public static const ACTIVE:int = 1;
		
		protected var icons:Array;
		protected var index:int;
		
		public function ToggleIconButton(icon1:BitmapData, icon2:BitmapData)
		{
			super(null);
			icons = new Array();
			icons.push(icon1);
			icons.push(icon2);
			init();
		}
		
		public function setState(s:int = ToggleIconButton.PASSIVE):void
		{
			index = ((s == ToggleIconButton.PASSIVE) || (s == ToggleIconButton.ACTIVE)) ? s : index;
			updateGraphics();
		}
		
		protected function init():void
		{
			updateGraphics();
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		protected function onClick(e:MouseEvent):void
		{
			index++;
			if (index >= icons.length)
				index = 0;
			updateGraphics();
		}
		
		protected function updateGraphics():void
		{
			var icon:BitmapData = icons[index];
			upState = getBitmap(icon);
			downState = getBitmap(icon);
			overState = getBitmap(icon);
			hitTestState = getBitmap(icon);
		}
	
	}

}