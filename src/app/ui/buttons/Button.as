package app.ui.buttons
{
	import app.misc.Config;
	import fl.controls.Button;
	
	/**
	 * Кнопка
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Button extends fl.controls.Button
	{
		
		public function Button()
		{
			super();
			setStyle('textFormat', Config.FLEX_TF.clone());
			setStyle('disabledTextFormat', Config.FLEX_TF.cloneDisabled());
		}
	
	}

}