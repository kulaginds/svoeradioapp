package app.ui.buttons
{
	import app.misc.Import;
	import app.ui.buttons.ToggleIconButton;
	
	/**
	 * Кнопка для добавления/удаления станции из/в закладки.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class BookmarkButton extends ToggleIconButton
	{
		public function BookmarkButton()
		{
			super(Import.add_bookmark, Import.remove_bookmark);
		}
	}
}