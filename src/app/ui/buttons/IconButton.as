package app.ui.buttons
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.SimpleButton;
	
	/**
	 * Кнопка-иконка
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class IconButton extends SimpleButton
	{
		
		public function IconButton(icon:BitmapData)
		{
			super(getBitmap(icon), getBitmap(icon), getBitmap(icon), getBitmap(icon));
		}
		
		protected function getBitmap(icon:BitmapData):Bitmap
		{
			return new Bitmap(icon);
		}
	
	}

}