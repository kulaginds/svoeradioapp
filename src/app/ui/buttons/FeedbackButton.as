package app.ui.buttons 
{
	import app.misc.Import;
	import flash.display.BitmapData;
	
	/**
	 * Кнопка обратной связи.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class FeedbackButton extends IconButton 
	{
		
		public function FeedbackButton() 
		{
			super(Import.feedback_btn);
			
		}
		
	}

}