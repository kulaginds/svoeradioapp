package app.ui.datagrid
{
	import app.misc.Config;
	import fl.controls.DataGrid;
	
	/**
	 * Таблица
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class DataGrid extends fl.controls.DataGrid
	{
		
		public function DataGrid()
		{
			super();
			sortableColumns = false;
			setStyle('headerTextFormat', Config.FLEX_TF.clone());
			setRendererStyle('textFormat', Config.FLEX_TF.clone());
			selectedIndex = -1;
		}
	
	}

}