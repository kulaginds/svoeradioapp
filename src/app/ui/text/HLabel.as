package app.ui.text
{
	import flash.text.TextField;
	
	/**
	 * Заголовочная надпись
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class HLabel extends TextField
	{
		protected var max_chars:int;
		
		public function HLabel(width:int, height:int, size:int, color:uint, max_chars:int)
		{
			super();
			
			selectable = false;
			this.width = width;
			this.height = height;
			
			var tf:TF = new TF();
			tf.size = size;
			tf.color = color;
			
			defaultTextFormat = tf;
			
			this.max_chars = max_chars;
		}
		
		public function setText(txt:String):void
		{
			text = String(txt).slice(0, max_chars);
			if (text.length != txt.length)
			{
				text += '...';
			}
			SvoeRadioApp.instance().tt.Register(this, txt);
		}
	}

}