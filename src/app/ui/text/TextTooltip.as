package app.ui.text
{
	import app.ui.text.TF;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * Класс который реализует
	 * отображение текстовой подсказки.
	 * Необходимо унаследоваться от HintSprite
	 * @author Serious Sam
	 */
	public class TextTooltip extends TooltipBase
	{
		// текстовое поле которое
		// будет отображать текст
		private var textField:TextField;
		
		public function TextTooltip(dx:int = 0, dy:int = 0)
		{
			super(dx, dy);
			//
			// создаем текстовое поле
			textField = new TextField();
			this.addChild(textField);
			textField.autoSize = TextFieldAutoSize.LEFT;
			textField.background = true;
			textField.border = true;
			textField.borderColor = 0xC0C0C0;
			textField.multiline = true;
			textField.defaultTextFormat = new TF();
		}
		
		/**
		 * Переопределяем метод который вызывается
		 * в базовом классе HintSprite
		 * в момент когад мышкой наводим
		 * на какой нибудь объект
		 * @param data - Информация которую надо показать
		 *
		 */
		protected override function showHint(data:Object):void
		{
			// отображает полученный текст
			textField.htmlText = data.toString();
		}
	}
}