package app.ui.text
{
	import app.ui.text.HLabel;
	
	/**
	 * Название трека.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class StationTrack extends HLabel
	{
		
		public function StationTrack()
		{
			super(340, 20, 14, 0x555555, 46);
		}
	}
}