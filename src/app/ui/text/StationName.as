package app.ui.text
{
	import app.ui.text.HLabel;
	
	/**
	 * Название станции.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class StationName extends HLabel
	{
		protected var playing:int = 0;
		protected var colors:Array = [0xC0C0C0, 0x3F3F3F];
		
		public function StationName()
		{
			super(197, 30, 24, 0xC0C0C0, 12);
		}
		
		public function play():void
		{
			playing = 1;
			render();
		}
		
		public function stop():void
		{
			playing = 0;
			render();
		}
		
		private function render():void
		{
			textColor = colors[playing];
		}
	}
}