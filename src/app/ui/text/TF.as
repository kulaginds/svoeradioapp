package app.ui.text
{
	import flash.text.Font;
	import flash.text.TextFormat;
	
	/**
	 * Формат текста в проекте.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class TF extends flash.text.TextFormat
	{
		public function TF()
		{
			super('Arial', 11, 0x000000);
			kerning = true;
		}
		
		public function clone():TF
		{
			return new TF();
		}
		
		public function cloneDisabled():TF
		{
			var tf:TF = new TF();
			tf.color = 0x999999;
			return tf;
		}
		
		public static function init():void
		{
			Font.registerFont(Arial);
		}
	}
}