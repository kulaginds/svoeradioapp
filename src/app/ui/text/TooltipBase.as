package app.ui.text
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	/**
	 * Базовый класс для отображения подсказки
	 * при наведении на спрайт
	 * @author Serious Sam
	 *
	 */
	public class TooltipBase extends MovieClip
	{
		// запоминаем ссылку на stage
		// что бы мы могли отображать и скрывать
		// подсказку в любой момент
		private static var stageLink:Stage;
		
		/**
		 * Инициализация компонента
		 * здесь надо запомнить ссылку на stage
		 * @param stageLink - Ссылка на stage
		 */
		public static function Init(stageLink:Stage):void
		{
			TooltipBase.stageLink = stageLink;
		}
		//
		//
		//
		// на сколько смещать подсказку
		// относительно координат мыши
		protected var delta:Point = new Point();
		// объект который ссылки на data 
		private var dictionary:Dictionary;
		
		public function TooltipBase(dx:int = 0, dy:int = 0)
		{
			// запиоминаем смещение
			delta = new Point(dx, dy);
			//
			// создаем нвоый словарь для хранения data 
			dictionary = new Dictionary();
			//
			// делаем окно с подсказкой недоступной для мыши
			this.mouseChildren = this.mouseEnabled = false;
		}
		
		// объект над которым сейчас находится мышка
		protected var currentTarget:DisplayObject;
		// объект data текущего объекта
		protected var currentData:Object;
		
		/**
		 * Обработчик события наведения мыши
		 */
		private function overHandler(event:MouseEvent):void
		{
			// определяем на каком объекте сейчас мышка
			currentTarget = event.currentTarget as DisplayObject;
			// добавляем компонент подсказки на stage
			stageLink.addChild(this);
			// задаем необходимы координаты
			setPosition();
			//
			// начинаем случать событие MOUSE_MOVE
			currentTarget.addEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
			//
			// определяем data из имеющегося словаря
			currentData = dictionary[currentTarget];
			//
			// метод showHint вызывается что бы потомки тоже могли
			// в этот момент сделать необходимые действия
			showHint(currentData);
			//
			// отправляем событие OPEN
			// смысл этого события тот же,
			// что и у метода showHint
			this.dispatchEvent(new Event(Event.OPEN));
		}
		
		/**
		 * Обработчик события отвода мыши от объекта
		 */
		private function outHandler(event:MouseEvent):void
		{
			// убираем компонент-подсказку со stage
			if (this.parent != null)
			{
				this.parent.removeChild(this);
			}
			//
			// убиваем слушателя события MOUSE_MOVE
			currentTarget.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandler);
		}
		
		/**
		 * Обработчик события движения мыши
		 */
		private function moveHandler(event:MouseEvent):void
		{
			// дергаем updateAfterEvent что бы
			// движение было более плавным
			event.updateAfterEvent();
			// задаем координаты компонента-подсказки
			setPosition();
		}
		
		/**
		 * Метод который задает текущие координаты
		 * для компонента-подсказки
		 * исходя из текущих коордирнат мыши
		 */
		private function setPosition():void
		{
			// задаем координаты
			// используя смещение delta
			this.x = stageLink.mouseX + delta.x;
			this.y = stageLink.mouseY + delta.y;
		}
		
		/**
		 * Зарегистрировать новый объект
		 * при наведении на который,
		 * должен показываться компонент-подсказка
		 * @param target - Целевой объект
		 * @param data - Информация которую надо показать
		 */
		final public function Register(target:DisplayObject, data:Object):void
		{
			// слушаем событие наведения мыши
			target.addEventListener(MouseEvent.ROLL_OVER, overHandler);
			// слушаем событие отвода мыши
			target.addEventListener(MouseEvent.ROLL_OUT, outHandler);
			// запоминаем в слвоаре ссылку на data
			dictionary[target] = data;
		}
		
		/**
		 * Метод который необходимо
		 * переопределить в под-классе
		 * @param data - Информация которую надо показать
		 */
		protected function showHint(data:Object):void  { ; }
	}
}