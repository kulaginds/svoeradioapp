package app.ui.text
{
	import app.misc.Config;
	import fl.controls.TextInput;
	
	/**
	 * Поле для ввода текста
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class TextInput extends fl.controls.TextInput
	{
		
		public function TextInput()
		{
			super();
			setStyle('textFormat', Config.FLEX_TF.clone());
			width = 150;
		}
	
	}

}