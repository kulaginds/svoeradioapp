package app.ui.containers
{
	import app.misc.Config;
	import app.ui.text.TF;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	/**
	 * Статусная строка.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class StatusLine extends Sprite
	{
		private var label:TextField;
		private var temp_message:String;
		private var moment_timer:Timer;
		
		public function StatusLine(stageWidth:int)
		{
			var w:int = stageWidth, h:int = 20;
			
			var m:Matrix = new Matrix();
			m.createGradientBox(w, h, Math.PI / 2, 25);
			
			graphics.beginGradientFill(GradientType.LINEAR, [0xEEEEEE, 0xDDDDDD], [1, 1], [0, 255], m);
			graphics.drawRect(0, 0, w, h);
			graphics.lineStyle(1, 0xC0C0C0);
			graphics.moveTo(0, h);
			graphics.lineTo(w, h);
			graphics.endFill();
			
			label = new TextField();
			label.selectable = false;
			label.width = 500;
			label.text = '';
			label.defaultTextFormat = new TF();
			label.x = 63;
			addChild(label);
			
			moment_timer = new Timer(Config.STATUS_MOMENT_TIME);
			moment_timer.addEventListener(TimerEvent.TIMER, onMomentComplete);
		}
		
		public function addMessage(s:String):void
		{
			label.text = s;
			moment_timer.stop();
		}
		
		public function addMomentMessage(s:String):void
		{
			if (!moment_timer.running) temp_message = label.text;
			label.text = s;
			moment_timer.stop();
			moment_timer.start();
		}
		
		private function onMomentComplete(e:TimerEvent):void
		{
			label.text = temp_message;
		}
	}
}