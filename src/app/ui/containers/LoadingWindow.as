package app.ui.containers
{
	import app.misc.Config;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * Окно загрузки приложения.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class LoadingWindow extends Sprite
	{
		private var box:Sprite;
		private var text:TextField;
		
		public function LoadingWindow(width:int, height:int, x:int = 0, y:int = 0)
		{
			super();
			
			var tf:TextFormat = Config.FLEX_TF.clone();
			tf.color = 0xFFFFFF;
			
			graphics.beginFill(0xFFFFFF, 0.6);
			graphics.drawRect(0, 0, width, height);
			graphics.endFill();
			
			box = new Sprite();
			box.graphics.beginFill(0x000000, 0.8);
			box.graphics.drawRoundRect((width - 80) / 2, (height - 50) / 2, 80, 50, 5, 5);
			box.graphics.endFill();
			addChild(box);
			
			text = new TextField();
			text.defaultTextFormat = tf;
			text.selectable = false;
			text.text = 'Загрузка...';
			text.x = (width - 80) / 2 + 10;
			text.y = (height - 50) / 2 + 15;
			addChild(text);
		}
		
		public function setError(msg:String):void
		{
			
			box.graphics.clear();
			box.graphics.beginFill(0x000000, 0.8);
			box.graphics.drawRoundRect((width - 200) / 2, (height - 100) / 2, 200, 100, 5, 5);
			box.graphics.endFill();
			
			text.wordWrap = true;
			text.selectable = true;
			text.text = msg;
			text.width = 180;
			text.height = 70;
			text.x = (width - 200) / 2 + 10;
			text.y = (height - 100) / 2 + 15;
			
			visible = true;
		}
	}
}