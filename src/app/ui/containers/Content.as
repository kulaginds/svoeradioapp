package app.ui.containers
{
	import app.misc.Import;
	import app.ui.tabs.BookmarksTab;
	import app.ui.tabs.CreateTab;
	import app.ui.tabs.SearchTab;
	import app.ui.tabs.TabButton;
	import app.ui.tabs.Tabs;
	
	/**
	 * Контент приложения.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Content extends Tabs
	{
		public var search_tab:SearchTab
		public var bookmarks_tab:BookmarksTab
		public var create_tab:CreateTab
		
		public function Content(width:int, height:int, tab:int = 0)
		{
			super(width, height);
			
			search_tab = new SearchTab();
			bookmarks_tab = new BookmarksTab();
			create_tab = new CreateTab();
			
			var search_btn:TabButton = new TabButton(Import.search_tab_deactive, Import.search_tab_active);
			var bookmarks_btn:TabButton = new TabButton(Import.bookmarks_tab_deactive, Import.bookmarks_tab_active);
			var create_btn:TabButton = new TabButton(Import.create_tab_deactive, Import.create_tab_active);
			
			var search_code:int = addTab(search_btn, search_tab),
				bookmarks_code:int = addTab(bookmarks_btn, bookmarks_tab),
				create_code:int = addTab(create_btn, create_tab);
				
			search_btn.value = search_code;
			bookmarks_btn.value = bookmarks_code;
			create_btn.value = create_code;
			
			search_tab.setCode(search_code);
			bookmarks_tab.setCode(bookmarks_code);
			create_tab.setCode(create_code);
			
			openTab(tab);
		}
	}
}