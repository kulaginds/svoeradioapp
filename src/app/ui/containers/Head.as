package app.ui.containers
{
	import app.misc.Import;
	import app.ui.buttons.AudioButton;
	import app.ui.buttons.BookmarkButton;
	import app.ui.buttons.IconButton;
	import app.ui.buttons.ToggleIconButton;
	import app.ui.rating.Rating;
	import app.ui.text.StationName;
	import app.ui.text.StationTrack;
	import app.ui.text.TextTooltip;
	import app.ui.volume.Volume;
	import flash.display.Sprite;
	
	/**
	 * Голова приложения.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class Head extends Sprite
	{
		protected var padding:int = 10;
		
		public var station_name:StationName;
		public var station_track:StationTrack;
		public var bookmark_btn:BookmarkButton;
		public var audio_btn:AudioButton;
		public var volume:Volume;
		public var play_btn:IconButton;
		public var stop_btn:IconButton;
		public var rating:Rating;
		
		public function Head(width:int, debug:Boolean)
		{
			var tt:TextTooltip = SvoeRadioApp.instance().tt;
			
			// Кнопка добавления/удаления закладок.
			bookmark_btn = new BookmarkButton();
			addChild(bookmark_btn);
			
			tt.Register(bookmark_btn, 'Добавить станцию в закладки');
			
			// Кнопка добавления трека в аудиозаписи.
			audio_btn = new AudioButton();
			audio_btn.x = bookmark_btn.x + bookmark_btn.width - audio_btn.width;
			audio_btn.y = bookmark_btn.y + bookmark_btn.height + padding + 3;
			audio_btn.visible = debug;
			addChild(audio_btn);
			
			tt.Register(audio_btn, 'Добавить трек в мои аудиозаписи');
			
			// Надпись названия станции.
			station_name = new StationName();
			station_name.x = bookmark_btn.x + bookmark_btn.width + padding;
			station_name.y = bookmark_btn.y;
			station_name.text = debug ? "StationName" : station_name.text;
			addChild(station_name);
			
			// Надпись трека станции.
			station_track = new StationTrack();
			station_track.x = station_name.x;
			station_track.y = audio_btn.y - 5;
			station_track.text = debug ? "StationTrack" : station_track.text;
			addChild(station_track);
			
			volume = new Volume(134, debug);
			volume.x = station_name.x + station_name.width + padding;
			volume.y = station_name.y + (station_name.height - volume.height) / 2;
			volume.value = 0.7;
			addChild(volume);
			
			// Кнопки управления воспроизведением.
			play_btn = new IconButton(Import.play);
			play_btn.x = volume.x + volume.width + padding;
			play_btn.y = station_name.y;
			addChild(play_btn);
			
			stop_btn = new IconButton(Import.stop);
			stop_btn.x = play_btn.x + play_btn.width + padding * 0.5;
			stop_btn.y = station_name.y;
			addChild(stop_btn);
			
			// Рейтинг.
			rating = new Rating(debug);
			rating.x = play_btn.x + 3;
			rating.y = play_btn.y + play_btn.height + padding * 0.5;
			addChild(rating);
		}
		
		public function stop():void
		{
			station_name.stop();
			audio_btn.visible = false;
			station_track.text = '';
		}
		
		public function play(track:String, is_bookmarked:Boolean = true):void
		{
			station_track.setText(track);
			bookmark_btn.setState(is_bookmarked ? ToggleIconButton.PASSIVE : ToggleIconButton.ACTIVE);
			audio_btn.visible = true;
		}
		
		public function setVolume(v:Number):void
		{
			volume.value = v;
		}
		
		public function init():void
		{
			volume.init();
		}
	}
}