package app.ui.containers 
{
	import app.misc.Config;
	import app.ui.buttons.Button;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * Окно с кнопкой "ОК".
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class MessageWindow extends Sprite 
	{
		private var box:Sprite;
		private var text:TextField;
		private var btn:Button;
		
		public function MessageWindow(width:int, height:int, x:int = 0, y:int = 0) 
		{
			super();
			
			var tf:TextFormat = Config.FLEX_TF.clone();
			tf.color = 0xFFFFFF;
			
			graphics.beginFill(0xFFFFFF, 0.6);
			graphics.drawRect(0, 0, width, height);
			graphics.endFill();
			
			box = new Sprite();
			addChild(box);
			
			text = new TextField();
			text.defaultTextFormat = tf;
			text.selectable = false;
			addChild(text);
			
			btn = new Button();
			btn.label = 'OK';
			addChild(btn);
			
			btn.addEventListener(MouseEvent.CLICK, onBtnClick);
			
		}
		
		public function setMessage(msg:String):void {
			var box_x:int = (width - 200) / 2,
				box_y:int = (height - 100 + btn.height + 10) / 2;
			
			box.graphics.clear();
			box.graphics.beginFill(0x000000, 0.8);
			box.graphics.drawRoundRect(box_x, box_y, 200, 100, 5, 5);
			box.graphics.endFill();
			
			text.wordWrap = true;
			text.selectable = true;
			text.text = msg;
			text.width = 180;
			text.height = 70;
			text.x = (width - 200) / 2 + 10;
			text.y = (height - 100) / 2 + 20;
			
			btn.x = box_x + (200 - btn.width) / 2;
			btn.y = box_y + 100 - btn.height - 10;
		}
		
		private function onBtnClick(e:MouseEvent):void {
			parent.removeChild(this);
		}
		
	}

}