package
{
	import app.managers.AudioManager;
	import app.managers.IOManager;
	import app.managers.KeepManager;
	import app.managers.UIManager;
	import app.misc.Config;
	import app.misc.Import;
	import app.misc.Messages;
	import app.ui.buttons.BugButton;
	import app.ui.buttons.FeedbackButton;
	import app.ui.containers.Content;
	import app.ui.containers.Head;
	import app.ui.containers.LoadingWindow;
	import app.ui.containers.MessageWindow;
	import app.ui.containers.StatusLine;
	import app.ui.text.TextTooltip;
	import app.ui.text.TF;
	import app.ui.text.TooltipBase;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.Font;
	import vk.api.serialization.json.JSON;
	
	/**
	 * Инициализирует все приложение.
	 * @author Dmitry Kulagin <kulaginds@gmail.com>
	 */
	public class SvoeRadioApp extends MovieClip
	{
		public static var flashVars:Object = {};
		
		public var loading_window:LoadingWindow; // окно загрузки - отображается когда происходит загрузка данных
		public var status_line:StatusLine; // статусная строка - отображает текущее состояние приложения
		public var head:Head; // голова приложения
		public var content:Content; // вкладки приложения
		public var feedback_btn:FeedbackButton; // кнопка обратной связи
		public var bug_btn:BugButton; // кнопка багов
		
		public var tt:TextTooltip; // инструмент для всплывающих подсказок
		
		private static var _instance:SvoeRadioApp; // ссылка на текущий экземпляр главного класса
		
		private var debug_graphics:Boolean;
		private var padding:int;
		
		public function SvoeRadioApp():void
		{
			_instance      = this;
			debug_graphics = false;
			padding        = 20;
			
			addEventListener(Event.ADDED_TO_STAGE, init);
			drawUI();
		}
		
		public static function instance():SvoeRadioApp
		{
			return _instance;
		}
		
		// инициализирует flashVars
		private function initFlashVars():void
		{
			// загрузка flashVars
			if (!flashVars.api_id) {
				flashVars = stage.loaderInfo.parameters;
			}
			
			// раскодировка api_result
			if ((flashVars.api_result != null) && (flashVars.api_result.length > 0))
			{
				flashVars.api_result = JSON.decode(flashVars.api_result).response;
			}
		}
		
		// загружает flashVars из конфига
		private function loadAppConfig():void
		{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, loadAppConfigComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loadAppConfigError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loadAppConfigError);
			loader.load(new URLRequest(Config.APP_CONFIG_URL));
		}
		
		private function afterLoadAppConfig():void
		{
			initToolTip();
			initFlashVars();
			initApp();
		}
		
		// обработчик события при загрузке конфига
		private function loadAppConfigComplete(e:Event):void
		{
			var config:Object = JSON.decode(e.target.data);
			flashVars         = config.flashVars;
			
			trace(JSON.encode(flashVars));
			
			afterLoadAppConfig();
		}
		
		// обработчик ошибки при загрузке конфига
		private function loadAppConfigError(e:Event):void
		{
			trace(Messages.ERROR_CONFIG_NOT_LOADED);
			afterLoadAppConfig();
		}
		
		// инициализирует toolTip
		private function initToolTip():void
		{
			TF.init(); // инициализация шрифта
			TooltipBase.Init(stage); // инициализация инструмента подсказок
		}
		
		// рисует графику приложения
		private function drawUI():void
		{
			// рисуем подсказку
			tt = new TextTooltip(10, 10);
			
			// установка фона приложения
			graphics.beginFill(0xF7F7F7);
			graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			graphics.endFill();
			
			// Фон интерфейса.
			var bg:Bitmap = new Bitmap(Import.background);
			bg.x = (stage.stageWidth - bg.width) / 2;
			bg.y = (stage.stageHeight - bg.height) / 2;
			addChild(bg);
			
			// Статусная строка.
			status_line = new StatusLine(stage.stageWidth);
			addChild(status_line);
			
			// Голова приложения.
			head = new Head((bg.width - padding * 2), debug_graphics);
			head.x = bg.x + padding;
			head.y = bg.y + padding;
			addChild(head);
			
			// Контент приложения.
			content = new Content(head.width, bg.height - padding * 2.25 - head.height);
			content.x = head.x;
			content.y = head.y + head.height + padding / 4;
			addChild(content);
			
			// Кнопка обратной связи
			feedback_btn = new FeedbackButton();
			feedback_btn.x = bg.x + padding;
			feedback_btn.y = bg.y + bg.height - 1;
			addChild(feedback_btn);
			
			// Кнопка сообщения багов.
			bug_btn = new BugButton();
			bug_btn.x = bg.x + bg.width - bug_btn.width - padding;
			bug_btn.y = feedback_btn.y;
			addChild(bug_btn);
			
			// Окно загрузки.
			loading_window = new LoadingWindow(stage.stageWidth, stage.stageHeight);
			addChild(loading_window);
		}
		
		// инициализация графики
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			trace("Initialization");
			
			loadAppConfig();
		}
		
		// запускает инициализацию контроллеров и графических элементов
		private function initApp():void
		{
			/*
			 * Инициализация контроллеров
			 */
			
			UIManager.init(); // взаимодействие с интерфейсом
			KeepManager.init(); // хранит важные данные
			AudioManager.init(); // управляет воспроизведением музыки
			IOManager.init(); // управляет взаимодействием с APIs
			
			// загрузка данных
			IOManager.firstLoad(); // подгрузка данных для приложения
			
			/*
			 * Инициализация графических элементов
			 */
			head.init();
		}
	}
}